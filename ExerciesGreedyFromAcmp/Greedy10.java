import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Greedy10 {
    static int inf = 1 << 27;
    static int[] parent;

    public static int[] mcmf(int[][] cap, int[][] cost, int src, int sink) {
        int totCost = 0, flow = 0;
        while (bellmanford(cap, cost, src, sink) != inf) {
            int[] temp = repair(cap, cost, src, sink);
            int newCost = temp[1];
            totCost += newCost;
            flow += temp[0];
        }
        return new int[] { flow, totCost };
    }

    private static int bellmanford(int[][] cap, int[][] cost, int src, int sink) {
        int n = cap.length;
        int[] dist = new int[n];
        Arrays.fill(dist, inf);
        dist[src] = 0;
        parent = new int[cap.length];
        Arrays.fill(parent, -1);
        boolean done = false;
        while (!done) {
            done = true;
            for (int u = 0; u < n; u++) {
                for (int v = 0; v < n; v++) {
                    if (cap[u][v] <= 0)
                        continue;
                    if (dist[v] > dist[u] + cost[u][v]) {
                        dist[v] = dist[u] + cost[u][v];
                        parent[v] = u;
                        done = false;
                    }
                }
            }
        }
        return dist[sink];
    }

    private static int[] repair(int[][] cap, int[][] cost, int src, int sink) {
        int flow = Integer.MAX_VALUE, pathCost = 0, current = sink;
        while (parent[current] != -1) {
            int from = parent[current], to = current;
            flow = Math.min(flow, cap[from][to]);
            pathCost += cost[from][to];
            current = parent[current];
        }
        current = sink;
        while (parent[current] != -1) {
            int from = parent[current], to = current;
            cap[from][to] -= flow;
            cap[to][from] += flow;
            current = parent[current];
        }
        return new int[] { flow, flow * pathCost };
    }

    public static void main(String[] args) throws Exception {
        InputReader r = new InputReader(new FileReader("INPUT.TXT"));
        PrintWriter out = new PrintWriter(new FileWriter("OUTPUT.TXT"));
        int n = r.nextInt();
        int[] arr = new int[n];
        int[][] cap  = new int[n + 2][n + 2];
        int[][] cost = new int[n + 2][n + 2];
        int src = n;
        int snk = src + 1;
        for (int i = 0; i < arr.length; i++) {
			arr[i] = r.nextInt();
			cap[src][i] = arr[i];
			cost[src][i] = 0;
			cap[i][snk] = 1;
			cost[i][snk] = 0;
			for(int j = 0;j < n;j++){
				int d = Math.abs(i - j);
				if(j > i)d = Math.min(d, i + n - j);
				else d = Math.min(d, j + n - i);
				cap[i][j] = 1;
				cost[i][j] = d;
			}
        }
        out.println(mcmf(cap, cost, src, snk)[1]);
        out.close();
    }

    static class InputReader {
        private BufferedReader reader;
        private StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream));
            tokenizer = null;
        }

        public InputReader(FileReader stream) {
            reader = new BufferedReader(stream);
            tokenizer = null;
        }

        public String nextLine() {
            try {
                return reader.readLine();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return null;
            }
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }
    }
}
