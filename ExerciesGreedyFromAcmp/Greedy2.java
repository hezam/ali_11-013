import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.StringTokenizer;
import java.math.BigInteger;
public class Greedy2 {
	static class Pair {
		int index;
		double value;
		Pair(double value, int index) {
			this.value = value;
			this.index = index;
		}
	}
	static int n;
	static double[] a, b;
	static ArrayList<Pair> id = new ArrayList<Pair>();
	static boolean ok(double x){
		double cur1 = x;
		int i = 0;
		while(i < n && cur1 >= a[id.get(i).index]){
			cur1 -= a[id.get(i).index];
			i++;
		}
		double cur2 = x;
		int j = n - 1;
		while(j >= 0 && cur2 >= b[id.get(j).index]){
			cur2 -= b[id.get(j).index];
			j--;
		}
		if(j < i)return true;
		if(j > i)return false;
		double r = cur1 / a[id.get(i).index]+ cur2 / b[id.get(i).index];
		return (r >= 1.0);
	}
	public static void main(String[] args) throws Exception {
		InputReader r = new InputReader(new FileReader("INPUT.TXT"));
        PrintWriter out = new PrintWriter(new FileWriter("OUTPUT.TXT"));
		n = r.nextInt();
		a = new double[n];
		b = new double[n];
		for(int i = 0;i < n;i++){
			a[i] = r.nextDouble();
			b[i] = r.nextDouble();
			id.add(new Pair((1.0 / a[i] - 1.0 / b[i]), i));
		}
		Collections.sort( id, (A, B) -> { 
				int tmp = (int) Math.round(A.value - B.value);
				return Integer.signum(tmp);
			 }
		 );
		double st = 0, en = 1000000000;
		for(int i = 0;i < 100;i++){
			double mid = (st + en) / 2;
			if(ok(mid))en = mid;
			else st = mid;
		}
		st = (double)Math.round(st * 1000d) / 1000d;
		out.println(st);
		for(int i = 0;i < n;i++)out.print((id.get(i).index + 1) + " ");
		out.println("");
		out.close();
	}
	
    static class InputReader {
        private BufferedReader reader;
        private StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream));
            tokenizer = null;
        }

        public InputReader(FileReader stream) {
            reader = new BufferedReader(stream);
            tokenizer = null;
        }

        public String nextLine() {
            try {
                return reader.readLine();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return null;
            }
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }
        
        public double nextDouble(){
			return Double.parseDouble(next());
		}
        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }
    }
}
