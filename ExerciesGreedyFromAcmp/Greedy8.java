import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.StringTokenizer;
import java.math.BigInteger;

public class Greedy8 {
	static class Pair {
		int value, index;
		Pair(int value, int index) {
			this.value = value;
			this.index = index;
		}
	}
	public static void main(String[] args) throws Exception {
		InputReader r = new InputReader(new FileReader("INPUT.TXT"));
        PrintWriter out = new PrintWriter(new FileWriter("OUTPUT.TXT"));
		int n = r.nextInt();
		int []a = new int[n];
		int []b = new int[n];
		Pair[]c = new Pair[n];
		for(int i = 0;i < n;i++){
			a[i] = r.nextInt();
			b[i] = r.nextInt();
			c[i] = new Pair(a[i] + b[i], i);
		}
		Arrays.sort(c, (A, B) -> B.value - A.value);
		int s1 = 0, s2 = 0;
		for(int i = 0;i < n;i++){
			if(i % 2 == 0){
				// First player
				s1 += a[c[i].index];
			}
			else s2 += b[c[i].index];
		}
		out.println(s1 - s2);
		out.close();
	}
    static class InputReader {
        private BufferedReader reader;
        private StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream));
            tokenizer = null;
        }

        public InputReader(FileReader stream) {
            reader = new BufferedReader(stream);
            tokenizer = null;
        }

        public String nextLine() {
            try {
                return reader.readLine();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return null;
            }
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }
    }
}
