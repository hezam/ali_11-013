import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.StringTokenizer;
import java.math.BigInteger;

public class Greedy3 {
	static int n, k;
	static boolean dp[][];
	static boolean vis[][];
	static int ans[];
	public static void main(String[] args) throws Exception {
		InputReader r = new InputReader(new FileReader("INPUT.TXT"));
        PrintWriter out = new PrintWriter(new FileWriter("OUTPUT.TXT"));
		n = r.nextInt();
		k = r.nextInt();
		if(k < 2 * n || k > 7 * n){
			out.println("NO SOLUTION");
			out.close();
			return;
		}
		dp = new boolean[n][k + 1];
		vis = new boolean[n][k + 1];
		ans = new int[n];
		for(int i = 0;i < n;i++){
			for(int j = 0;j < k + 1;j++){
				vis[i][j] = false;
			}
		}
		goMin(0, k);
		for(int i = 0;i < n;i++)out.print(ans[i]);
		out.println("");
		for(int i = 0;i < n;i++){
			for(int j = 0;j < k + 1;j++){
				vis[i][j] = false;
			}
		}
		goMax(0, k);
		for(int i = 0;i < n;i++)out.print(ans[i]);
		out.println("");
		out.close();
	}
	static int get(int d){
		if(d == 0)return 6;
		if(d == 1)return 2;
		if(d == 2)return 5;
		if(d == 3)return 5;
		if(d == 4)return 4;
		if(d == 5)return 5;
		if(d == 6)return 6;
		if(d == 7)return 3;
		if(d == 8)return 7;
		return 6;
	}
	static boolean goMin(int i, int rem){
		if(i == n)return rem == 0;
		if(rem < 0)return false;
		if(vis[i][rem])return dp[i][rem];
		vis[i][rem] = true;
		for(int d = 0;d <= 9;d++){
			if(d == 0 && rem == k)continue;
			if(goMin(i + 1, rem - get(d))){
				ans[i] = d;
				return dp[i][rem] = true;
			}
		}
		return dp[i][rem] = false;
	}
	static boolean goMax(int i, int rem){
		if(i == n)return rem == 0;
		if(rem < 0)return false;
		if(vis[i][rem])return dp[i][rem];
		vis[i][rem] = true;
		for(int d = 9;d >= 0;d--){
			if(d == 0 && rem == k)continue;
			if(goMax(i + 1, rem - get(d))){
				ans[i] = d;
				return dp[i][rem] = true;
			}
		}
		return dp[i][rem] = false;
	}
	    static class InputReader {
        private BufferedReader reader;
        private StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream));
            tokenizer = null;
        }

        public InputReader(FileReader stream) {
            reader = new BufferedReader(stream);
            tokenizer = null;
        }

        public String nextLine() {
            try {
                return reader.readLine();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return null;
            }
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }
    }
}
