import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.StringTokenizer;
import java.math.BigInteger;

public class Greedy7 {
	public static void main(String[] args) throws Exception {
		InputReader r = new InputReader(new FileReader("INPUT.TXT"));
        PrintWriter out = new PrintWriter(new FileWriter("OUTPUT.TXT"));
		int k = r.nextInt();
		String str = r.next();
		char []s = str.toCharArray();
		int n = str.length();
		boolean []used = new boolean[n];
		for(int i = 0;i < n;i++)used[i] = false;
		if(k == 0){
			out.print(str);
			out.close();
			return;
		}
		for(int i = 0;i < n;i++){
			if(i >= k && !used[i - k]){
				used[i - k] = true;
				out.print(s[i - k]);
				continue;
			}
			int idx = -1;
			for(int j = Math.max(0, i - k); j <= Math.min(n - 1, i + k);j++){
				if(used[j])continue;
				if(idx == -1 || s[idx] > s[j]){
					idx = j;
				}
			}
			out.print(s[idx]);
			used[idx] = true;
		}
		out.println("");
		out.close();
	}
	
	static class InputReader {
        private BufferedReader reader;
        private StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream));
            tokenizer = null;
        }

        public InputReader(FileReader stream) {
            reader = new BufferedReader(stream);
            tokenizer = null;
        }

        public String nextLine() {
            try {
                return reader.readLine();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return null;
            }
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }
    }
}
