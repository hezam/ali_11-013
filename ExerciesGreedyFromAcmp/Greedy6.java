import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.StringTokenizer;
import java.math.BigInteger;

public class Greedy6 {
	static int getRank(char c){
		if(c == '6')return 0;
		if(c == '7')return 1;
		if(c == '8')return 2;
		if(c == '9')return 3;
		if(c == 'T')return 4;
		if(c == 'J')return 5;
		if(c == 'Q')return 6;
		if(c == 'K')return 7;
		return 8;
	}
	public static void main(String[] args) throws Exception {
		InputReader r = new InputReader(new FileReader("INPUT.TXT"));
        PrintWriter out = new PrintWriter(new FileWriter("OUTPUT.TXT"));
		int n = r.nextInt();
		int m = r.nextInt();
		char trump = r.next().charAt(0);
		String []a = new String[n];
		String []b = new String[m];
		boolean []used = new boolean[n];
		for(int i = 0;i < n;i++){
			a[i] = r.next();
			used[i] = false;
		}
		for(int i = 0;i < m;i++){
			b[i] = r.next();
		}
		for(int i = 0;i < m;i++){
			int rank1 = getRank(b[i].charAt(0));
			char suit1 = b[i].charAt(1);
			int best = 9;
			int idx = -1;
			for(int j = 0;j < n;j++){
				if(used[j])continue;
				char suit2 = a[j].charAt(1);
				if(suit1 != suit2)continue;
				int rank2 = getRank(a[j].charAt(0));
				if(rank1 > rank2)continue;
				if(rank2 < best){
					best = rank2;
					idx = j;
				}
			}
			if(idx != -1){
				used[idx] = true;
				continue;
			}
			if(suit1 == trump){
				out.print("NO");
				out.close();
				return;
			}
			// Get lowest trump
			for(int j = 0;j < n;j++){
				if(used[j])continue;
				char suit2 = a[j].charAt(1);
				if(suit2 != trump)continue;
				int rank2 = getRank(a[j].charAt(0));
				if(rank2 < best){
					best = rank2;
					idx = j;
				}
			}
			if(idx != -1){
				used[idx] = true;
				continue;
			}
			out.print("NO");
			out.close();
			return;
		}
		out.println("YES");
		out.close();
	}
    static class InputReader {
        private BufferedReader reader;
        private StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream));
            tokenizer = null;
        }

        public InputReader(FileReader stream) {
            reader = new BufferedReader(stream);
            tokenizer = null;
        }

        public String nextLine() {
            try {
                return reader.readLine();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return null;
            }
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }
    }
}
