import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.StringTokenizer;
import java.math.BigInteger;

public class Greedy5 {
	static class Pair {
		int value, index;
		Pair(int value, int index) {
			this.value = value;
			this.index = index;
		}
	}
	static int n, s;
	static int[] a, b;
	static boolean[] used;
	static ArrayList<Integer> ans = new ArrayList<Integer>();
	static ArrayList<Integer> res = new ArrayList<Integer>();
	static void go(int x){
		if(a[x] > s)return;
		int r = s, u = 0;
		while(u < res.size() && (r + b[res.get(u)] >= a[x])){
			u += 1;
			r += b[res.get(u)];
		}
		r+=b[x];
		for(int i = u; i < res.size();i++){
			if(a[res.get(i)] > r)return;
			r += b[res.get(i)];
		}
		ArrayList<Integer> tmp = new ArrayList<Integer>();
		for(int i = 0;i < u;i++)tmp.add(res.get(i));
		tmp.add(x);
		for(int i = u;i < res.size();i++){
			tmp.add(res.get(i));
		}
		res = tmp;
	}
	public static void main(String[] args) throws Exception {
		InputReader r = new InputReader(new FileReader("INPUT.TXT"));
        PrintWriter out = new PrintWriter(new FileWriter("OUTPUT.TXT"));
		n = r.nextInt();
		s = r.nextInt();
		a = new int[n + 1];
		b = new int[n + 1];
		used = new boolean[n + 1];
		for(int i = 1;i <= n;i++){
			a[i] = r.nextInt();
			b[i] = r.nextInt();
		}
		for(int j = 0;j < n;j++){
			for(int i = 1;i <= n;i++){
				if(used[i] == false && (a[i] <= s) && b[i] >= 0){
					used[i] = true;
					s += b[i];
					ans.add(i);
				}
			}
		}
		ArrayList<Pair> v = new ArrayList<Pair>();
		for(int i = 1;i <= n;i++){
			if(b[i] < 0)v.add(new Pair(b[i], i));
		}
		Collections.sort(v, (A, B) -> B.value - A.value);
		for(int i = 0;i < v.size();i++)go(v.get(i).index);
		out.println((ans.size() + res.size()));
		for(int i = 0;i < ans.size();i++)out.print(ans.get(i) + " ");
		for(int i = 0;i < res.size();i++)out.print(res.get(i) + " ");
		out.println("");
		out.close();
	}
	static class InputReader {
        private BufferedReader reader;
        private StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream));
            tokenizer = null;
        }

        public InputReader(FileReader stream) {
            reader = new BufferedReader(stream);
            tokenizer = null;
        }

        public String nextLine() {
            try {
                return reader.readLine();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return null;
            }
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }
    }
}
