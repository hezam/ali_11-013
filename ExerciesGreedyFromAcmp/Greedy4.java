import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.StringTokenizer;
import java.math.BigInteger;

public class Greedy4 {
	static class Pair {
		int value, index;
		Pair(int value, int index) {
			this.value = value;
			this.index = index;
		}
	}
	public static void main(String[] args) throws Exception {
		InputReader re = new InputReader(new FileReader("INPUT.TXT"));
        PrintWriter out = new PrintWriter(new FileWriter("OUTPUT.TXT"));
		int n = re.nextInt();
		int r = re.nextInt();
		if(r == 1){
			for(int i = 0;i < n;i++){
				int x = re.nextInt();
				int y = re.nextInt();
				if(y > x){
					out.println("Impossible");
					out.close();
					return;
				}
			}
			return;
		}
		int[] w = new int[n];
		int[] d = new int[n];
		Pair[] id = new Pair[n];
		for(int i = 0;i < n;i++){
			w[i] = re.nextInt();
			d[i] = re.nextInt();
			id[i] = new Pair(d[i], i);
		}
		Arrays.sort(id, (A, B) -> B.value - A.value);
		int cur = 0;
		for(int i = 0;i < n;i++){
			int j = id[i].index;
			int p = (w[j] - d[j]) / (r - 1);
			if(p <= 0)continue;
			if(cur + p - 1 > d[j]){
				out.println("Impossible");
				out.close();
				return;
			}
			cur += p;
		}
		cur = 0;
		for(int i = 0;i < n;i++){
			int j = id[i].index;
			int p = (w[j] - d[j]) / (r - 1);
			if(p <= 0)continue;
			out.println(cur + " " + (j + 1));
			cur += p;
		}
		out.close();
	}
    static class InputReader {
        private BufferedReader reader;
        private StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream));
            tokenizer = null;
        }

        public InputReader(FileReader stream) {
            reader = new BufferedReader(stream);
            tokenizer = null;
        }

        public String nextLine() {
            try {
                return reader.readLine();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return null;
            }
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public long nextLong() {
            return Long.parseLong(next());
        }
    }
}
