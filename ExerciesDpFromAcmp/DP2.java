import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.StringTokenizer;
public class DP2 {
	static int dp[];
	static int a[];
	static int n;
	public static void main(String[] args) {
		FastScanner fs = new FastScanner();
		n = fs.nextInt();
		dp = new int[n];
		a = new int[n];
		for(int i = 0;i < n;i++){
			dp[i] = -1;
			a[i] = fs.nextInt();
		}
		System.out.println(cal(0));
	}
	static int cal(int i){
		if(i == n - 1)return 0;
		if(dp[i] != -1)return dp[i];
		int ret = 1000000000;
		if(i == n - 2)ret = Math.abs(a[i] - a[i + 1]) + cal(i + 1);
		else ret = Math.min(Math.abs(a[i] - a[i + 1]) + cal(i + 1), 
							3 * Math.abs(a[i] - a[i + 2]) + cal(i + 2));
		return dp[i] = ret;
	}
	static class FastScanner {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st=new StringTokenizer("");
		String next() {
			while (!st.hasMoreTokens())
				try {
					st=new StringTokenizer(br.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
			return st.nextToken();
		}
		
		int nextInt() {
			return Integer.parseInt(next());
		}
		int[] readArray(int n) {
			int[] a=new int[n];
			for (int i=0; i<n; i++) a[i]=nextInt();
			return a;
		}
		long nextLong() {
			return Long.parseLong(next());
		}
	}
}
