import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.StringTokenizer;

public class DP9 {
	static int dp[][], a[][], b[];
	static int row, col, st, en, sum;
	public static void main(String[] args) {
		FastScanner fs = new FastScanner();
		row = fs.nextInt();
		col = fs.nextInt();
		dp = new int[row][col];
		a = new int[row][col];
		b = new int[row];
		for(int i = 0;i < row;i++){
			for(int j = 0;j < col;j++){
				a[i][j] = fs.nextInt();
			}
		}
		int ans = -1000000000;
		for(int l = 0;l < col;l++){
			for(int i = 0;i < row;i++){
				b[i] = 0;
			}
			for(int r = l;r < col;r++){
				for (int i = 0; i < row;i++){
					b[i] += a[i][r];
				}
				ans = Math.max(ans, kadaneAlgo());
			}
		}
		System.out.println(ans);
	}
	static int kadaneAlgo(){
		int sum = 0, maxSum = -1000000000;
		en = -1;
		int tempStart = 0;
		for (int i = 0; i < row; i++) {
			sum += b[i];
			if (sum < 0) {
				sum = 0;
				tempStart = i+1;
			} else if (sum > maxSum) {
				maxSum = sum;
				st = tempStart;
				en = i;
			}
		}
		if (en != -1)return maxSum;
		maxSum = b[0];
		st = en = 0;
		for (int i = 1; i < row; i++) {
			if (b[i] > maxSum) {
				maxSum = b[i];
				st = en = i;
			}
		}
		return maxSum;
	}
	
	static class FastScanner {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st=new StringTokenizer("");
		String next() {
			while (!st.hasMoreTokens())
				try {
					st=new StringTokenizer(br.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
			return st.nextToken();
		}
		
		int nextInt() {
			return Integer.parseInt(next());
		}
		int[] readArray(int n) {
			int[] a=new int[n];
			for (int i=0; i<n; i++) a[i]=nextInt();
			return a;
		}
		long nextLong() {
			return Long.parseLong(next());
		}
	}
}
