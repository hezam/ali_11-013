import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.StringTokenizer;
public class DP10 {
	static int dp[][];
	static int a[][];
	static int n, m;
	public static void main(String[] args) {
		FastScanner fs = new FastScanner();
		n = fs.nextInt();
		m = fs.nextInt();
		dp = new int[n][m];
		a = new int[n][m];
		for(int i = 0;i < n;i++){
			for(int j = 0;j < m;j++){
				dp[i][j] = -1;
				a[i][j] = fs.nextInt();
			}
		}
		System.out.println(cal(0, 0));
	}
	static int cal(int i, int j){
		if(i == n - 1 && j == m - 1)return a[i][j];
		if(i == n || j == m)return 1000000000;
		if(dp[i][j] != -1)return dp[i][j];
		int ret = a[i][j];
		ret += Math.min(cal(i + 1, j), cal(i, j + 1));
		return ret;
	}
	static class FastScanner {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st=new StringTokenizer("");
		String next() {
			while (!st.hasMoreTokens())
				try {
					st=new StringTokenizer(br.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
			return st.nextToken();
		}
		
		int nextInt() {
			return Integer.parseInt(next());
		}
		int[] readArray(int n) {
			int[] a=new int[n];
			for (int i=0; i<n; i++) a[i]=nextInt();
			return a;
		}
		long nextLong() {
			return Long.parseLong(next());
		}
	}
}
