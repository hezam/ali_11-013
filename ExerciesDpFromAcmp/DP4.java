import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.StringTokenizer;
import java.math.BigInteger;
public class DP4 {
	static BigInteger dp[][];
	static int n, m;
	static BigInteger A = new BigInteger("-1");
	public static void main(String[] args) {
		FastScanner fs = new FastScanner();
		n = fs.nextInt();
		m = fs.nextInt();
		if(m > n){
			int temp = m;
			m = n;
			n = temp;
		}
		// m <= 6
		dp = new BigInteger[n][1<<m];
		for(int i = 0;i < n;i++){
			for(int j = 0;j < 1<<m;j++){
				dp[i][j] = A;
			}
		}
		BigInteger ans = new BigInteger("0");
		for(int mask = 0; mask < (1 <<m);mask++){
			ans = ans.add(cal(1, mask));
		}
		System.out.println(ans);
	}
	static BigInteger cal(int row, int mask){
		if(row == n)return new BigInteger("1");
		if(dp[row][mask].compareTo(A) != 0)return dp[row][mask];
		BigInteger ret = new BigInteger("0");
		for(int newMask = 0; newMask < (1<<m);newMask++){
			boolean valid = true;
			for(int i = 1; i < m;i++){
				int c = 0;
				if((mask&(1<<i)) != 0)c++;
				if((mask&(1<<(i-1))) != 0)c++;
				
				if((newMask&(1<<i)) != 0)c++;
				if((newMask&(1<<(i-1))) != 0)c++;
				if(c == 4 || c == 0){
					valid = false;
					break;
				}
			}
			if(valid){
				ret = ret.add(cal(row + 1, newMask));
			}
		}
		return dp[row][mask] = ret;
	}
	static class FastScanner {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st=new StringTokenizer("");
		String next() {
			while (!st.hasMoreTokens())
				try {
					st=new StringTokenizer(br.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
			return st.nextToken();
		}
		
		int nextInt() {
			return Integer.parseInt(next());
		}
		int[] readArray(int n) {
			int[] a=new int[n];
			for (int i=0; i<n; i++) a[i]=nextInt();
			return a;
		}
		long nextLong() {
			return Long.parseLong(next());
		}
	}
}
