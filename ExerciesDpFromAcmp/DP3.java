import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.StringTokenizer;
public class DP3 {
	static int dp[][][];
	static int a[];
	static int n;
	public static void main(String[] args) {
		FastScanner fs = new FastScanner();
		n = fs.nextInt();
		dp = new int[n][n][2];
		a = new int[n];
		for(int i = 0;i < n;i++){
			a[i] = fs.nextInt();
			dp[i][i][0] = a[i];
			dp[i][i][1] = 0;
		}
		for(int l = 2;l <= n;l++){
			for(int i = 0;i <= n - l;i++){
				int j = l + i - 1;
				int left  = a[i] + dp[i + 1][j][1];
				int right = a[j] + dp[i][j - 1][1];
				if(left > right){
					dp[i][j][0] = left;
					dp[i][j][1] = dp[i + 1][j][0];
				}
				else{
					dp[i][j][0] = right;
					dp[i][j][1] = dp[i][j - 1][0];
				}
			}
		}
		int ans = dp[0][n - 1][0] - dp[0][n - 1][1];
		if(ans > 0)System.out.println(1);
		else if(ans == 0)System.out.println(0);
		else System.out.println(2);
	}
	static int cal(int l, int r, int turn){
		return 0;
	}
	static class FastScanner {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st=new StringTokenizer("");
		String next() {
			while (!st.hasMoreTokens())
				try {
					st=new StringTokenizer(br.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
			return st.nextToken();
		}
		
		int nextInt() {
			return Integer.parseInt(next());
		}
		int[] readArray(int n) {
			int[] a=new int[n];
			for (int i=0; i<n; i++) a[i]=nextInt();
			return a;
		}
		long nextLong() {
			return Long.parseLong(next());
		}
	}
}
