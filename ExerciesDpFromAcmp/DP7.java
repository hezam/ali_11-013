import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.StringTokenizer;
public class DP7 {
	static int dp[][];
	static char a[][];
	static int n;
	public static void main(String[] args) {
		FastScanner fs = new FastScanner();
		n = fs.nextInt();
		dp = new int[n][n];
		a = new char[n][n];
		for(int i = 0;i < n;i++){
			String str = fs.next();
			a[i] = str.toCharArray();
			for(int j = 0;j < n;j++){
				if(a[i][j] == '1')dp[i][j] = 1;
				else dp[i][j] = 0;
				if(i > 0)dp[i][j] += dp[i - 1][j];
				if(j > 0)dp[i][j] += dp[i][j - 1];
				if(i > 0 && j > 0)dp[i][j] -= dp[i - 1][j - 1];
			}
		}
		int ans = 0;
		for(int i = 0;i < n;i++){
			for(int j = 0;j < n;j++){
				int st = 0, en = n;
				while(st + 1 < en){
					int mid = (st + en) / 2;
					int x1 = i, y1 = j;
					int x2 = i + mid - 1, y2 = j + mid - 1;
					
					if(ok(x1, y1, x2, y2))st = mid;
					else en = mid;
				}
				while(st <= n && ok(i, j, i + st, j + st))st++;
				ans = Math.max(ans, st);
			}
		}
		System.out.println(ans * ans);
	}
	
	static int get(int x1, int y1, int x2, int y2){
		int ret = dp[x2][y2];
		if(x1 > 0)ret -= dp[x1 - 1][y2];
		if(y1 > 0)ret -= dp[x1][y1 - 1];
		if(x1 > 0 && y1 > 0)ret += dp[x1 - 1][y1 - 1];
		return ret;
	}
	static boolean ok(int x1, int y1, int x2, int y2){
		if(x2 >= n || y2 >= n)return false;
		int l = x2 - x1 + 1;
		return get(x1, y1, x2, y2) == l * l;
	}
	
	
	static class FastScanner {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st=new StringTokenizer("");
		String next() {
			while (!st.hasMoreTokens())
				try {
					st=new StringTokenizer(br.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
			return st.nextToken();
		}
		
		int nextInt() {
			return Integer.parseInt(next());
		}
		int[] readArray(int n) {
			int[] a=new int[n];
			for (int i=0; i<n; i++) a[i]=nextInt();
			return a;
		}
		long nextLong() {
			return Long.parseLong(next());
		}
	}
}
