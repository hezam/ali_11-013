import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.StringTokenizer;
public class DP8 {
	static long dp[][];
	static int n, k;
	public static void main(String[] args) {
		FastScanner fs = new FastScanner();
		n = fs.nextInt();
		k = fs.nextInt();
		
		dp = new long[n][2];
		for(int i = 0;i < n;i++){
			dp[i][0] = -1;
			dp[i][1] = -1;
		}
		System.out.println(cal(0, 1));
	}
	static long cal(int i, int pre){
		if(i == n)return 1;
		if(dp[i][pre] != -1)return dp[i][pre];
		dp[i][pre] = 0;
		for(int d = 0;d < k;d++){
			if(i == 0 && d == 0)continue;	// leading zero
			if(d == 0 && pre == 0)continue;
			if(d == 0)dp[i][pre] += cal(i + 1, 0);
			else dp[i][pre] += cal(i + 1, 1);
		}
		return dp[i][pre];
	}
	static class FastScanner {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st=new StringTokenizer("");
		String next() {
			while (!st.hasMoreTokens())
				try {
					st=new StringTokenizer(br.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
			return st.nextToken();
		}
		
		int nextInt() {
			return Integer.parseInt(next());
		}
		int[] readArray(int n) {
			int[] a=new int[n];
			for (int i=0; i<n; i++) a[i]=nextInt();
			return a;
		}
		long nextLong() {
			return Long.parseLong(next());
		}
	}
}
