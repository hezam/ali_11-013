import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.StringTokenizer;
public class DP6 {
	static boolean dp[][];
	static boolean vis[][];
	static char[] word;
	static char[] pattern;
	static int n, m;
	public static void main(String[] args) {
		FastScanner fs = new FastScanner();
		
		String str = fs.next();
		n = str.length();
		word = str.toCharArray();
		
		str = fs.next();
		m = str.length();		
		pattern = str.toCharArray();
		
		dp = new boolean[n][m];
		vis = new boolean[n][m];
		for(int i = 0;i < n;i++){
			for(int j = 0;j < m;j++){
				vis[i][j] = false;
			}
		}
		boolean ans = cal(0, 0);
		if(ans)System.out.println("YES");
		else System.out.println("NO");
	}
	static boolean cal(int i, int j){
		if(i == n && j == m)return true;
		if(i == n || j == m)return false;
		if(vis[i][j])return dp[i][j];
		vis[i][j] = true;
		boolean ret = false;
		if(word[i] == pattern[j])
		return dp[i][j] = cal(i + 1, j + 1);
		if(pattern[j] == '?'){
			// Change the '?' in the patteren to English letter
			return dp[i][j] = cal(i + 1, j + 1);
		}
		if(pattern[j] == '*'){
			for(int k = i;k <= n;k++){
				dp[i][j] |= cal(k, j + 1);
			}
			return dp[i][j];
		}
		return false;
	}
	static class FastScanner {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st=new StringTokenizer("");
		String next() {
			while (!st.hasMoreTokens())
				try {
					st=new StringTokenizer(br.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
			return st.nextToken();
		}
		
		int nextInt() {
			return Integer.parseInt(next());
		}
		int[] readArray(int n) {
			int[] a=new int[n];
			for (int i=0; i<n; i++) a[i]=nextInt();
			return a;
		}
		long nextLong() {
			return Long.parseLong(next());
		}
	}
}
