import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.StringTokenizer;
import java.math.BigInteger;
public class DP1 {
	static BigInteger dp[][];
	static int n, k;
	public static void main(String[] args) {
		FastScanner fs = new FastScanner();
		k = fs.nextInt();
		n = fs.nextInt();
		dp = new BigInteger[n + 1][k + 1];
		for(int i = 0;i < n + 1;i++){
			for(int j = 0;j < k + 1;j++){
				dp[i][j] = new BigInteger("-1");
			}
		}
		System.out.println(cal(0, 0));
	}

	static BigInteger A = new BigInteger("-1");

	static BigInteger cal(int i, int pre){
		if(i == n)return new BigInteger("1");
		if(dp[i][pre].compareTo(A) != 0)return dp[i][pre];
		BigInteger ret = new BigInteger("0");
		if(pre != k && pre != 0)ret = ret.add(cal(i + 1, pre + 1));
		ret = ret.add(cal(i + 1, 1));
		dp[i][pre] = ret;
		return ret;
	}
	
	static class FastScanner {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st=new StringTokenizer("");
		String next() {
			while (!st.hasMoreTokens())
				try {
					st=new StringTokenizer(br.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
			return st.nextToken();
		}
		
		int nextInt() {
			return Integer.parseInt(next());
		}
		int[] readArray(int n) {
			int[] a=new int[n];
			for (int i=0; i<n; i++) a[i]=nextInt();
			return a;
		}
		long nextLong() {
			return Long.parseLong(next());
		}
	}
}
