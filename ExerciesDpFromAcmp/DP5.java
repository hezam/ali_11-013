import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.StringTokenizer;
import java.math.BigInteger;
public class DP5 {
	static BigInteger dp[][][];
	static int n;
	static BigInteger A = new BigInteger("-1");
	public static void main(String[] args) {
		FastScanner fs = new FastScanner();
		n = fs.nextInt();
		dp = new BigInteger[n][451][451];
		for(int i = 0;i < n;i++){
			for(int j = 0;j < 451;j++){
				for(int k = 0 ;k < 451;k++){
					dp[i][j][k] = A;
				}
			}
		}
		System.out.println(cal(0, 0, 0));
	}
	static BigInteger cal(int i, int s1, int s2){
		if(i == n){
			if(s1 == s2)return new BigInteger("1");
			return new BigInteger("0");
		}
		if(dp[i][s1][s2].compareTo(A) != 0)return dp[i][s1][s2];
		BigInteger ret = new BigInteger("0");
		for(int d = 0;d < 10;d++){
			if(i < n / 2)ret = ret.add(cal(i + 1, s1 + d, s2));
			else ret = ret.add(cal(i + 1, s1, s2 + d));
		}
		dp[i][s1][s2] = ret;
		return ret;
	}
	
	static class FastScanner {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st=new StringTokenizer("");
		String next() {
			while (!st.hasMoreTokens())
				try {
					st=new StringTokenizer(br.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
			return st.nextToken();
		}
		
		int nextInt() {
			return Integer.parseInt(next());
		}
		int[] readArray(int n) {
			int[] a=new int[n];
			for (int i=0; i<n; i++) a[i]=nextInt();
			return a;
		}
		long nextLong() {
			return Long.parseLong(next());
		}
	}
}
