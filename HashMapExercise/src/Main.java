import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {

        String text;

        text = "hezamali";

        Map<Character , Integer> numOfChar = new HashMap<>();
        for (int i = 0; i < text.length(); i++)
        {
            if (numOfChar.containsKey(text.charAt(i)))
            {
                int a = numOfChar.get(text.charAt(i));
                a++;
                numOfChar.replace(text.charAt(i), a);
            }
            else
            {
                numOfChar.put(text.charAt(i), 1);
            }
        }
        System.out.println(numOfChar);

    }
}
