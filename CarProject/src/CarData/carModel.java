package CarData;
//ModelId,Maker,Model
public class carModel {

    private int modelId;
    private String maker;
    private int model;

    @Override
    public String toString() {
        return "carModel{" +
                "modelId=" + modelId +
                ", maker='" + maker + '\'' +
                ", model=" + model +
                '}';
    }

    public int getModelId() {
        return modelId;
    }

    public String getMaker() {
        return maker;
    }

    public int getModel() {
        return model;
    }
}