package CarData;
public class Car {

   private int idCar;
   private String nameCar;
   private  String fullName;
   private int idCountry;
   private String Country;
   private int idContinent;
   private String Continent;

   public Car() {
   }

   public Car(int idCar, String nameCar, String fullName, int idCountry, String country, int idContinent, String continent) {
      this.idCar = idCar;
      this.nameCar = nameCar;
      this.fullName = fullName;
      this.idCountry = idCountry;
      Country = country;
      this.idContinent = idContinent;
      Continent = continent;
   }

   public void setIdCar(int idCar) {
      this.idCar = idCar;
   }

   public String getNameCar() {
      return nameCar;
   }

   public void setNameCar(String nameCar) {
      this.nameCar = nameCar;
   }

   public String getFullName() {
      return fullName;
   }

   public void setFullName(String fullName) {
      this.fullName = fullName;
   }

   public int getIdCountry() {
      return idCountry;
   }

   public void setIdCountry(int idCountry) {
      this.idCountry = idCountry;
   }

   public String getCountry() {
      return Country;
   }

   public void setCountry(String country) {
      Country = country;
   }

   public int getIdContinent() {
      return idContinent;
   }

   public void setIdContinent(int idContinent) {
      this.idContinent = idContinent;
   }

   public String getContinent() {
      return Continent;
   }

   public void setContinent(String continent) {
      Continent = continent;
   }


}
