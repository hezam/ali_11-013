package CarData;
//CountryId,CountryName,Continent
public class Country {

    private int countryId;
    private String countryName;
    private Continent continent;

    public Country(int iD, String name, Continent c) {
        this.countryId=iD;
        this.countryName=name;
        this.continent=c;
    }


    @Override
    public String toString() {
        return "Country{" +
                "countryId=" + countryId +
                ", countryName=" + countryName  +
                ", continent=" + continent +
                '}';
    }

    public int getCountryId() {
        return countryId;
    }

    public String getCountryName() {
        return countryName;
    }

    public Continent getContinent() {
        return continent;
    }
}
