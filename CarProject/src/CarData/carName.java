package CarData;
//Id,Model,Make
public class carName {
    private int carId ;
    private int model;
    private String make;

    public carName(int carId, int model, String make) {
        this.carId = carId;
        this.model = model;
        this.make = make;
    }

    public int getCarId() {
        return carId;
    }

    public int getModel() {
        return model;
    }

    public String getMake() {
        return make;
    }
}
