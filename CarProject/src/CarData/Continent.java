package CarData;
//ContId,Continent
public class Continent {
    private int contId;
    private String ContinentName;


    @Override
    public String toString() {
        return "Continent{" +
                "contId=" + contId +
                ", ContinentName='" + ContinentName + '\'' +
                '}';
    }

    public Continent(int contId, String continentName) {
        this.contId = contId;
        this.ContinentName = continentName;
    }

    public int getContId() {
        return contId;
    }




}
