package CarData;
//Id,Maker,FullName,Country
public class carMakers {

    private int id ;
    private String maker;
    private String fullName;
    private Country country;

    public carMakers(int id, String maker, String fullName, Country country) {
        this.id = id;
        this.maker = maker;
        this.fullName = fullName;
        this.country = country;
    }

    @Override
    public String toString() {
        return "carMakers{" +
                "id=" + id +
                ", maker='" + maker + '\'' +
                ", fullName='" + fullName + '\'' +
                ", country=" + country +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMaker() {
        return maker;
    }

    public void setMaker(String maker) {
        this.maker = maker;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}
