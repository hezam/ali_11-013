package CarData;

//import com.opencsv.CSVReader;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

   static List<Continent> continents = new ArrayList<>();

    public static void main(String[] args) throws FileNotFoundException {
	// write your code here


        String pathModelList= "CarProject/model-list.csv";
        String pathCarData="CarProject/cars-data.csv";
        String pathContinents = "CarProject/continents.csv";
        String pathCountries = "CarProject/countries.csv";
        String pathCarName = "CarProject/car-names.csv";
        String pathCarMakers = "CarProject/car-makers.csv";



        Scanner sc = new Scanner(new File(pathContinents));

        sc.nextLine(); //skip first Line
        while (sc.hasNextLine()){
            String[] lineData = sc.nextLine().split(",");

            Continent  c = new Continent(
                    Integer.parseInt(lineData[0]),
                    lineData[1].substring(1 , lineData[1].length()  -1 )
            );
            continents.add(c);
        }
             System.out.println(continents);

        //reading countries
        Country c = new Country(
                15,"Brazil" ,getContinentByID(1)
        );
        System.out.println(c);
        c.getContinent();



//1-2
       withdrawAllManufacturers("countries.csv");
//5
       averageHorsepower("cars-data.csv");
    }

    private static Continent getContinentByID(int iD) {
        for(Continent c : continents){
            if (c.getContId()==iD){
                return c;


            }
        }
        return null;
    }


    public  static  List<String> withdrawAllManufacturers(String path){


        String line = "";

        String[] allManufacturers;
        List<String> listAllManufacturersInEurope = new ArrayList<>() ;
        List<String> listAllManufacturersInAsia = new ArrayList<>() ;
        List<String> listAllManufacturersInAmerica = new ArrayList<>() ;
        List<String> listAllManufacturersInAfrica = new ArrayList<>() ;
        List<String> listAllManufacturersInAustralia = new ArrayList<>() ;

        try {
            BufferedReader br = new BufferedReader(new FileReader(path));//قراه الملف
            br.readLine();
            while ((line = br.readLine()) != null){
                allManufacturers = line.split(",");
                if (allManufacturers[2].equals("2")){
                    listAllManufacturersInEurope.add(allManufacturers[1]) ;

                }else if (allManufacturers[2].equals("3")){
                    listAllManufacturersInAsia.add(allManufacturers[1]);

                }else if (allManufacturers[2].equals("1")){
                    listAllManufacturersInAmerica.add(allManufacturers[1]);
                }else if (allManufacturers[2].equals("4")){
                    listAllManufacturersInAfrica.add(allManufacturers[1]);
                }else {
                    listAllManufacturersInAustralia.add(allManufacturers[1]);

                }

            }
            System.out.println("All manufacturers are in America" + " , And maximum number in America :" +listAllManufacturersInAmerica.size());
            System.out.println(listAllManufacturersInAmerica);
            System.out.println("All manufacturers are in Europe" + " , And maximum number in Europe :" +listAllManufacturersInEurope.size());
            System.out.println(listAllManufacturersInEurope);
            System.out.println("All manufacturers are in Asia" + "  ,And maximum number in Asia :" + listAllManufacturersInAsia.size());
            System.out.println(listAllManufacturersInAsia);
            System.out.println("All manufacturers are in Africa" + "  ,And maximum number in Africa :" + listAllManufacturersInAfrica.size());
            System.out.println(listAllManufacturersInAfrica);
            System.out.println("All manufacturers are in Australia" + "  ,And maximum number in Australia :" + listAllManufacturersInAfrica.size());
            System.out.println(listAllManufacturersInAustralia);


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listAllManufacturersInEurope;
    }

    public static  List <Integer> averageHorsepower( String path){
        //المتغيرات
        int averageHorsePower ;
        int sum = 0;
        int count=0;
        String line = "";
        String[] horsePower; //مصفوفه لتخزين الاعداد بشكل نصي

        List<Integer> listHorsepower = new ArrayList<>();//ليست لتخزين الاعداد بشكل عددي
        try {
            BufferedReader br = new BufferedReader(new FileReader(path));//قراه الملف
            br.readLine();
            while ((line = br.readLine()) != null ) { //قراءه الملف سطر سطر
                horsePower = line.split(","); //تقسيم السطر
                if (horsePower[4].equalsIgnoreCase("null")){ //اذا صادف نص غير عددي يقوم بتحويلها لصفر
                    horsePower[4] = "0";
                }
                listHorsepower.add(Integer.valueOf(horsePower[4])); //تحويل العدد من شكل نصي الى شكل عددي واضافته لليست
                sum += (Integer.parseInt(horsePower[4]));//زياده العدد عل المجموع
                count++;
            }
            averageHorsePower =  sum / count; //حساب المتوسط
           // System.out.println(listHorsepower);
            System.out.println("Average HorsePower is :" +averageHorsePower );
            System.out.println("count is "+count);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listHorsepower ;
    }





   public static List<String> readCSV(String path){
        BufferedReader br = null;
        String[] cd = null;
        String line ="";
        String split=",";
        List<String> list = new ArrayList<String>();
        try {
            br=new BufferedReader(new FileReader(path));
            while ((line = br.readLine()) != null) {
                boolean firstLine = true;
                while (line != null && line.startsWith("Horsepower")) {
                    if (firstLine) {
                        firstLine = false;
                        continue;
                    } else {
                        list.add(line);
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return list;
    }





}
