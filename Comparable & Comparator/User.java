import java.util.*;
public class User implements Comparable<User> {

    private  int year;
    private String name;

    public User(int year, String name) {
        this.year = year;
        this.name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int compareTo(User user) {
        if (this.year>user.year){
            return 1;
        }else if (this.year<user.year){
            return -1;
        }
        return 0;
    }
}
