import java.util.Comparator;

public class UserComo  implements Comparator<User> {


    @Override
    public int compare(User o1, User o2) {

        if (o1.getYear()> o2.getYear()){
            return 1;
        }else if (o1.getYear()< o2.getYear()){
            return -1;
        }
        return 0;
    }

}
