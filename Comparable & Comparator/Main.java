import java.io.*;
import java.util.*;

public class Main {
    public static void main(String[] args) {




        ArrayList<User> users = new ArrayList<>();

        users.add(new User(2001 , "Ali"));
        users.add(new User(2002 , "Ahmed"));
        users.add(new User(1998 , "Hezam"));
        users.add(new User(2000 , "Salem"));

        // Comparable
        Collections.sort(users);

        for (User user: users
        ) {
            System.out.println(user.getYear());
        }

        System.out.println("-------------------");
        //with Comparator

        UserComo como = new UserComo();
        Collections.sort(users,como);

        for (User user: users
        ) {
            System.out.println(user.getYear());

        }


        



    }
}

