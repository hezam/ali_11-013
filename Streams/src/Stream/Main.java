package Stream;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) throws IOException {



        String imageUrl = "https://servernews.ru/assets/external/illustrations/2012/09/11/735735/image_gallery.jpg";
        String destinationFile = "image.jpg";
        saveImage(imageUrl, destinationFile);

        create();
        copy(new File("src/Stream.file.txt"), new File("src/Stream.fileCopy.txt"));
        copyBuffered(new File("src/Stream.file.txt"), new File("src/Stream.fileCopyBuffered.txt"));

        createNewFile("URLPhoto.img",
                "src/Stream.file.txt",
                URL("https://miro.medium.com/max/8642/1*iIXOmGDzrtTJmdwbn7cGMw.png"));

        createNewFile("dataHTML",
                "src/Stream.file.txt",
                URL("https://beginnersbook.com/2014/07/java-program-for-binary-to-decimal-conversion/"));



        String urlAddress = "https://en.wikipedia.org/wiki/State_of_Palestine";
        String extension = ".pdf";
        URL url = new URL(urlAddress);
        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));

        String line = br.readLine();


        while (line != null) {
            System.out.println(line);
            List<String> urlofExtension = findAllFilesofExtension(line, extension);
            for (String u : urlofExtension) {

                System.out.println(u);
                String filename = "blabla" + extension;
               // download(u, filename);
            }
            line = br.readLine();
        }

    }

    private static List<String> findAllFilesofExtension(String line, String extension) {
        List<String> urls = new ArrayList<>();
        Pattern p = Pattern.compile("\"[^\"]+\\." + extension + "\"");
        line = line.toLowerCase();
        Matcher m = p.matcher(line);
        while (m.find()) {
        urls.add(m.group());
    }

        return urls;
    }

    public static void create() throws IOException {
        File file1 = new File("src/Stream.file.txt");
        FileOutputStream put = new FileOutputStream(file1);
        Random r = new Random();
        for (int i = 0; i < 100; i++) {
            int w = r.nextInt(128);
            put.write(w);
        }
        put.close();

    }

    public static void copy(File from, File to) throws IOException {
        FileInputStream stream = new FileInputStream(from);
        FileOutputStream put = new FileOutputStream(to);

        int c;
        while ((c = stream.read()) != -1) {
            put.write(c);
        }
        stream.close();
        put.close();
    }

    private static String URL(String Link) throws IOException {
        String data = "";

        var url = new URL(Link);
        InputStream is = url.openStream();
        int c;
        while ((c = is.read()) != -1) {
            data += (char) c;
        }
        is.close();
        return data;
    }

    public static void saveImage(String imageUrl, String destinationFile) throws IOException {
        URL url = new URL(imageUrl);
        InputStream is = url.openStream();
        OutputStream os = new FileOutputStream(destinationFile);

        byte[] b = new byte[2048];
        int length;

        while ((length = is.read(b)) != -1) {
            os.write(b, 0, length);
        }

        is.close();
        os.close();
    }

    private static void createNewFile(String nameOfFile, String path, String url) throws IOException {
        FileWriter fileWriter1 = new FileWriter(nameOfFile);
        fileWriter1.write(path);
        fileWriter1.write(url);
        fileWriter1.close();
    }

    public static void copyBuffered(File from, File to) throws IOException {

        BufferedReader bR = new BufferedReader(new FileReader(from));
        BufferedWriter bW = new BufferedWriter(new FileWriter(to));

        int i;
        while ((i = bR.read()) != -1) {
            bW.write(i);

        }

        bW.close();
        bR.close();

    }
}
