public class Main {

    public static void main(String[] args) {
	// write your code here
        MinMax sList = new MinMax();

        //Adds data to the list
        sList.addNode(5);
        sList.addNode(8);
        sList.addNode(1);
        sList.addNode(6);

        //Display the minimum value node in the list
        sList.minNode();

        //Display the maximum value node in the list
        sList.maxNode();
    }
}

