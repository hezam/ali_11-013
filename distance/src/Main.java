import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here


        MyPoint myPoint = new MyPoint();

        Scanner scanner = new Scanner(System.in);

        System.out.println(" Enter the num of x1");
        myPoint.setX1(scanner.nextDouble());
        System.out.println(" Enter the num of x2");
        myPoint.setX2(scanner.nextDouble());
        System.out.println(" Enter the num of y1");
        myPoint.setY1(scanner.nextDouble());
        System.out.println(" Enter the num of y2");
        myPoint.setY2(scanner.nextDouble());

        Distance(myPoint.getX1(), myPoint.getX2(), myPoint.getY1(), myPoint.getY2());


    }

     public static double Distance (double x1 , double x2 , double y1 , double y2 ){
        double  DistanceBetween2Points;

        DistanceBetween2Points = Math.sqrt((x2-x1) * (x2-x1) + (y2-y1) * (y2-y1));

         System.out.println(DistanceBetween2Points);
        return DistanceBetween2Points;
    }
}
