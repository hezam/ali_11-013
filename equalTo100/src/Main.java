import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        int n ;
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the number of elements you want to store: ");
        //reading the number of elements from the that we want to
        n = scanner.nextInt();

        int[] array= new int[n];
        System.out.println("Enter the elements of the array: ");
        for(int i=0; i<n; i++)
        {
            //reading array elements from the user
            array[i]=scanner.nextInt();
        }
        System.out.println(check(array));

    }


    public static boolean check(int[] a){
        int cont;
        cont = 0;
        for (int i = 0; i <a.length; i++) {
            if (a[i]== 100){
                cont++;
            }
            if(cont > 2){
                return false;
            }
        }
        if(cont == 2){
            return true;
        }
        return false;
    }
}
