public class Main {


    static int sum = 0;
    static class Node
    {
        int data;
        Node next;
    }

    static Node push( Node head_ref, int new_data)
    {
        // allocate node /
        Node new_node = new Node();

        // put in the data /
        new_node.data = new_data;

        // link the old list to the new node /
        new_node.next = (head_ref);

        // move the head to point to the new node /
        (head_ref) = new_node;
        return head_ref;
    }

    // function to recursively find the sum of
    // nodes of the given linked list
    static void sumOfNodes( Node head)
    {
        // if head = null
        if (head == null)
            return;

        // recursively traverse the remaining nodes
        sumOfNodes(head.next);

        // accumulate sum
        sum = sum + head.data;
    }

    // utility function to find the sum of nodes
    static int sumOfNodesUtil( Node head)
    {

        sum = 0;

        // find the sum of nodes
        sumOfNodes(head);

        // required sum
        return sum;
    }
    public static void main(String[] args) {
        // write your code here
        Node head = null;

        // create linked list 8.7.6.5.4
        head = push(head, 8);
        head = push(head, 7);
        head = push(head, 6);
        head = push(head, 5);
        head = push(head, 4);

        System.out.println( "Sum of nodes = "
                + sumOfNodesUtil(head));

    }
}
