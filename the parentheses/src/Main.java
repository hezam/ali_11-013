
public class Main {
    public static void main(String[] args) {
        String a1 = "(2+3)*(2-4)";
        System.out.println(check(a1));
    }
    // without Stack
    private static boolean check(String a) {
        int open=0;
        for (int i = 0; i <a.length() ; i++) {
            if (a.charAt(i)=='('){
                open++;
            }
            if (a.charAt(i)==')'){
                open--;
            }
        }
        return open ==0;
    }
}
