import java.util.*;

public class Main {

    public static void main(String[] args) {
	// write your code here


        Collection<Integer> listOne = Arrays.asList(4,3,2);
        Collection<Integer> listTwo = Arrays.asList(2,3,4,5);


        Collection<Integer> similar = new HashSet<Integer>(listOne);
        Collection<Integer> different = new HashSet<>();
        different.addAll(listOne);
        different.addAll(listTwo);

        similar.retainAll(listTwo);
        different.removeAll(similar);


        System.out.println( "List One "+listOne +"\n" +"ListTwo " + listTwo);
        System.out.println( "Similar :" + similar + "\n" + "Different" +different);
    }
}
